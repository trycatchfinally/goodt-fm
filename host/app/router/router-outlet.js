export class RouterOutlet extends HTMLElement {

    constructor() {
        super();
    }

    connectedCallback() {
        let template = `
              <div id="router-outlet">Router outlet ....</div>
        `;
        this.innerHTML = template;
    }
}
