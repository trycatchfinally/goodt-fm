import {eventService} from '../events/eev-service';
import {e}            from '../events/event-names';

export class NavHelper {

    static trigger(link) {
        if (!(link && link.hasAttribute('href'))) return;
        eventService.emit(e.router.navigate.trigger, link.getAttribute('href'));
    };

    static extractRootRoute(route, base = false) {
        let parts = (route.indexOf('#') === -1 ? route : route.slice(1)).split('/').filter(v => v !== '');
        return base ? parts[0] : parts.join('/');
    };
}

export class NavTriggerElement extends HTMLElement {

    constructor() {
        super();
        this.$links = null;
    }

    connectedCallback() {
        this.$links = this.querySelectorAll('a[href^="#"]');
        this.$links.forEach(link => {
            link.addEventListener('click', $e => this.trigger($e, link));
        });
    }

    disconnectedCallback() {
        this.$links.forEach(link => {
            link.removeEventListener('click', $e => this.trigger($e, link));
        });
    }

    trigger($e, link) {
        $e.preventDefault();
        NavHelper.trigger(link);
    }
}
