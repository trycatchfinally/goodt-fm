import {HomeComponent}     from '../components/home.comp';
import {AboutComponent}    from '../components/about.comp';
import {PostsAppComponent} from '../components/posts-app.comp';
import {PagesAppComponent} from '../components/pages-app.comp';
import {MixedComponent}    from '../components/mixed.comp';
import {MediaAppComponent} from '../components/media-app.comp';
import Navigo from 'navigo';

export class RouterHandler {

    constructor() {
        if (!RouterHandler.instance) {
            RouterHandler.instance = this;
        } else {
            throw new Error('use getInstance');
        }

        let root    = null;
        let useHash = true;
        let hash    = '#';
        this.router = new Navigo(root, useHash, hash);
        return RouterHandler.instance;
    }

    static get getInstance() {
        return RouterHandler.instance;
    }

    static inject(component) {
        const outlet = document.querySelector('router-outlet');
        while (outlet.firstChild) {
            outlet.removeChild(outlet.firstChild);
        }
        outlet.appendChild(component);
    }

    init() {
        const routes = [
            {path: 'about', resolve: AboutComponent},
            {path: 'posts/*', resolve: PostsAppComponent},
            {path: 'posts', resolve: PostsAppComponent},
            {path: 'pages', resolve: PagesAppComponent},
            {path: 'mixed', resolve: MixedComponent},
            {path: 'media/*', resolve: MediaAppComponent},
            {path: 'media', resolve: MediaAppComponent}
        ];

        this.router.on(() => {
            RouterHandler.inject(new HomeComponent())
        }).resolve();

        routes.forEach(route => {
            this.router.on(
                route.path,
                (params) => {
                    RouterHandler.inject(new route.resolve(params))
                },
                {
                    before: (done, params) => {
                        if (!route.canActivate || route.canActivate()) {
                            done();
                        } else {
                            this.router.navigate('/');
                            done(false);
                        }
                    }
                }
            ).resolve();
        });

       this.router.notFound(() => {
            console.log('404');
       });
    }
}

RouterHandler.instance = null;

