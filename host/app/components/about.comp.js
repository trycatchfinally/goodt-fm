import tmpl                from './about.html';
import data                from '../../package';
import {NavTriggerElement} from '../router/nav-helper';
import {eventService}      from '../events/eev-service';
import {e}                 from '../events/event-names';

export class AboutComponent extends NavTriggerElement {

    constructor() {
        super();
    }

    static get observedAttributes() {
        return ['package-details'];
    }

    connectedCallback() {
        this.innerHTML = tmpl;
        this.$package  = this.querySelector('[data-package]');
        this.$details  = this.querySelector('pre');

        eventService.emit(e.notification.type.updated);
        this.setAttribute('package-details', JSON.stringify(data, null, '\t'));

        super.connectedCallback();
    }

    disconnectedCallback() {
        super.disconnectedCallback();
    }

    attributeChangedCallback(name, oldVal, newVal) {
        switch (name) {
            case 'package-details':
                if (newVal) {
                    this.$details.innerText = newVal;
                    this.$package.setAttribute('data-package', 'true');
                }
                break;
        }
    }
}
