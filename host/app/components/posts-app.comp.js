import tmpl                      from './posts-app.html';
import {e}                       from '../events/event-names';
import {eventService}            from '../events/eev-service';
import {AureliaAdapterComponent} from './adapters/aurelia-adapter.comp';

export class PostsAppComponent extends AureliaAdapterComponent {

    constructor() {
        super(tmpl);
    }

    connectedCallback() {
        eventService.emit(e.notification.type.updated, 'Aurelia');
        super.connectedCallback();
    }
}
