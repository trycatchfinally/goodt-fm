import tmpl                  from './media-app.html';
import {eventService}        from '../events/eev-service';
import {e}                   from '../events/event-names';
import {VueAdapterComponent} from './adapters/vue-adapter.comp';

export class MediaAppComponent extends VueAdapterComponent {

    constructor() {
        super(tmpl);
    }

    connectedCallback() {
        eventService.emit(e.notification.type.updated, 'Vue');
        super.connectedCallback();
    }
}
