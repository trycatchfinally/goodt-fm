import {RouterHandler}     from '../router/router-handler';
import {NavTriggerElement} from '../router/nav-helper';
import {eventService}      from '../events/eev-service';
import {e}                 from '../events/event-names';
import tmpl                from './home.html';

export class HomeComponent extends NavTriggerElement {

    constructor() {
        super();
        this.innerHTML = tmpl;
        RouterHandler.getInstance.router.navigate('#/');
    }

    static get observedAttributes() {
        return [];
    }

    connectedCallback() {
        eventService.emit(e.notification.type.updated);
        super.connectedCallback();
    }

    disconnectedCallback() {
        super.disconnectedCallback();
    }
}
