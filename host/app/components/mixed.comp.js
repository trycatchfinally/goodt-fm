import tmpl from './mixed.html';

// todo: fix aurelia loader and refresh, see console log
export class MixedComponent extends HTMLElement {
    constructor() {
        super();
        this.innerHTML = tmpl;
        let $container, $div, $posts, $pages, $media;

        $container = this.querySelector('.row');
        $div       = document.createElement('div');
        $div.classList.add('col-xs-6');

        $posts = $div.cloneNode();
        $pages = $div.cloneNode();
        $media = $div.cloneNode();

        $posts.appendChild(document.createElement('pages-app'));
        $pages.appendChild(document.createElement('posts-app'));
        $media.appendChild(document.createElement('media-app'));

        //$container.appendChild($media);
        $container.appendChild($posts);

        setTimeout(() => $container.appendChild($pages), 300);
    }
}
