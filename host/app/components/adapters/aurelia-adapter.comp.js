import {AbstractAdapterComponent} from './abstract-adapter.comp';
import {eventService}             from '../../events/eev-service';
import {e}                        from '../../events/event-names';


// For SystemJS only
export class AureliaAdapterComponent extends AbstractAdapterComponent {

    constructor(template, bundle, root) {
        super(template, bundle, root);
    }

    attributeChangedCallback(name, oldValue, newValue) {
        switch (name) {
            case 'initialized':
                if (newValue === 'true') {
                    let $au = this.querySelector(this.root);

                    // todo: remove attr on demand?
                    let attrs = $au.getAttributeNames().filter(e => e.indexOf('au') === 0);
                    if (Array.isArray(attrs) && attrs.length) $au.removeAttribute(attrs[0]);

                    setTimeout(() => $au.aurelia.root.viewModel.outerEventService = eventService, 0);
                }
                break;
        }
    }

    bootstrap() {
        console.log('AureliaAdapterComponent: Aurelia based component loaded');
        SystemJS.import('aurelia-bootstrapper')
            .then(() => {
                this.subscriptions[e.notification.outer.event] = () => console.log(`AureliaAdapterComponent: event ${e.notification.outer.event} detected`);
                eventService.on(e.notification.outer.event, this.subscriptions[e.notification.outer.event]);
                console.log(`AureliaAdapterComponent: subscribed on ${e.notification.outer.event} event`);
            })
            .catch(function (err) { console.log(err); });
    }
}
