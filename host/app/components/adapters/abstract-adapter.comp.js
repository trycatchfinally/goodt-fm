import {ComponentDispatcher} from '../../core/dispatcher';
import {eventService}        from '../../events/eev-service';
import {e}                   from '../../events/event-names';

export class AbstractAdapterComponent extends HTMLElement {

    // template
    // bundle
    // root

    constructor(template, bundle, root) {
        super();

        let config = ComponentDispatcher.extract(this.tagName);
        if (config) {
            bundle = config.hasOwnProperty('bundle') && !bundle ? config.bundle : bundle;
            root   = config.hasOwnProperty('root') && !root ? config.root : root;
        }

        this.template = template;
        this.bundle   = bundle;
        this.root     = root;

        this.subscriptions = {};
        console.log(`AbstractAdapterComponent: current tagName is ${this.tagName}`);
    }

    static get observedAttributes() {
        return ['initialized'];
    }

    connectedCallback() {
        if (!this.hasAttribute('initialized')) {
            this.setAttribute('initialized', 'false');
        }

        this.subscriptions[e.router.extract.root] = data => {
            this.setAttribute('base-url', data);
            this.init();
        };

        eventService.on(e.router.extract.root, this.subscriptions[e.router.extract.root]);
        eventService.emit(e.router.request.root);
    }

    disconnectedCallback() {
        if (this.subscriptions.hasOwnProperty(e.notification.outer.event)) {
            eventService.off(e.notification.outer.event, this.subscriptions[e.notification.outer.event]);
            console.log(`AbstractAdapterComponent: ${this.tagName} unsubscribed from ${e.notification.outer.event} event`);
        }
        eventService.off(e.router.extract.root, this.subscriptions[e.router.extract.root]);
        this.subscriptions = {};
    }

    init() {
        if (!this.bundle) return;

        const bundleScript = document.createElement('script');
        const template     = document.createElement('template');

        template.innerHTML = this.template;
        this.appendChild(template.content.cloneNode(true));

        bundleScript.setAttribute('src', this.bundle);
        this.appendChild(bundleScript);

        bundleScript.onload = () => {
            this.bootstrap();
        };
    }

    // todo: Promise?
    bootstrap() {
        // method required
    }
}
