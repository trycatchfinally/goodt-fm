import {AbstractAdapterComponent} from './abstract-adapter.comp';
import {eventService}             from '../../events/eev-service';
import {e}                        from "../../events/event-names";

export class VueAdapterComponent extends AbstractAdapterComponent {

    constructor(template, bundle, root) {
        super(template, bundle, root);
    }

    attributeChangedCallback(name, oldValue, newValue) {
        switch (name) {
            case 'initialized':
                if (newValue === 'true') {
                    try {
                        let $v = document.querySelector(this.root).__vue__;

                        $v.$children[0].outerEventService = eventService;

                    } catch (e) {
                    }
                }
                break;
        }
    }

    bootstrap() {
        console.log('VueAdapterComponent: Vue based component loaded');
        try {
            this.subscriptions[e.notification.outer.event] = () => console.log(`VueAdapterComponent: event ${e.notification.outer.event} detected`);
            eventService.on(e.notification.outer.event, this.subscriptions[e.notification.outer.event]);
            console.log(`VueAdapterComponent: subscribed on ${e.notification.outer.event} event`);
        } catch (e) {
        }
    }
}
