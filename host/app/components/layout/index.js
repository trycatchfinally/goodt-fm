import {CNavComponent}      from './c-nav.comp';
import {CFooterComponent}   from './c-footer.comp';
import {CNotifierComponent} from './c-notifier.comp';

export const layoutComponents = [
    {
        tagName  : 'c-nav',
        component: CNavComponent
    },
    {
        tagName  : 'c-footer',
        component: CFooterComponent
    },
    {
        tagName  : 'c-notifier',
        component: CNotifierComponent
    }
];
