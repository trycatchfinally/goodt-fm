import tmpl           from './c-notifier.html';
import {eventService} from '../../events/eev-service';
import {e}            from '../../events/event-names';

export class CNotifierComponent extends HTMLElement {

    constructor() {
        super();
    }

    static get observedAttributes() {
        return ['type'];
    }

    attributeChangedCallback(name, oldValue, newValue) {
        switch (name) {
            case 'type':
                this.$type.innerText = newValue;
                this.$type.setAttribute('data-app', newValue.toLowerCase());
                break;
        }
    }

    connectedCallback() {
        this.innerHTML    = tmpl;
        this.$type        = this.querySelector('span');
        this.subscription = type => this.update(type);

        eventService.on(e.notification.type.updated, this.subscription);
        if (!this.hasAttribute('type')) {
            this.update();
        }
    }

    disconnectedCallback() {
        eventService.off(e.notification.type.updated, this.subscription);
        this.subscription = null;
    }

    update(type) {
        type      = type || 'WebComponent';
        this.type = type;
    }

    get type() {
        return this.getAttribute('type');
    }

    set type(newValue) {
        this.setAttribute('type', newValue);
        setTimeout(() => document.title = newValue, 0);
    }
}
