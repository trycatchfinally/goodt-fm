import {RouterHandler} from '../../router/router-handler';
import {NavHelper}     from '../../router/nav-helper';
import {eventService}  from '../../events/eev-service';
import {e}             from '../../events/event-names';
import tmpl            from './c-nav.html';

export class CNavComponent extends HTMLElement {
    constructor() {
        super();

        this.navigate     = this.navigate.bind(this);
        this.$links       = null;
        this.rootUrl      = null;
        this.subscriptions = {};
    }

    static get observedAttributes() {
        return [];
    }

    connectedCallback() {
        this.innerHTML = tmpl;

        this.$links = this.querySelectorAll('a[data-navigo]');
        this.$links.forEach(link => {
            link.addEventListener('click', e => this.navigate(e, link));
        });
        this.setCurrentActive();

        this.subscriptions[e.router.navigate.trigger] = (target) => {
            RouterHandler.getInstance.router.navigate(target);
            this.updateActive(target);
        };
        this.subscriptions[e.router.request.root] = () => {
            eventService.emit(e.router.extract.root, this.rootUrl);
        };

        eventService.on(e.router.navigate.trigger, this.subscriptions[e.router.navigate.trigger]);
        eventService.on(e.router.request.root, this.subscriptions[e.router.request.root]);
    }

    disconnectedCallback() {
        this.$links.forEach(link => {
            link.removeEventListener('click', e => this.navigate(e, link));
        });

        eventService.off(e.router.navigate.trigger, this.subscriptions[e.router.navigate.trigger]);
        eventService.off(e.router.request.root, this.subscriptions[e.router.request.root]);

        this.subscriptions = {};
    }

    navigate(e, link) {
        e.preventDefault();
        let routingTo = link.getAttribute('href');
        RouterHandler.getInstance.router.navigate(routingTo);
        this.updateActive(routingTo);
    }

    setCurrentActive() {
        let curUrl = location.hash;
        this.updateActive(curUrl);
    }

    updateActive(route) {
        let links = this.querySelectorAll('a[data-navigo]');

        links.forEach(link => {
            let linkRoute = link.getAttribute('href');
            if (linkRoute === route) {
                link.classList.add('active');
                this.rootUrl = NavHelper.extractRootRoute(route);
            } else {
                link.classList.remove('active');
            }
        });

        if (!this.rootUrl) {
            this.rootUrl = NavHelper.extractRootRoute(route, true);
        }
    }

    createNavItemLink(href, content) {
        const liNavItem     = document.createElement('li');
        liNavItem.className = 'nav-item';

        const aNavLink     = document.createElement('a');
        aNavLink.classList = 'nav-link';
        aNavLink.href      = href;
        aNavLink.innerHTML = content;

        liNavItem.appendChild(aNavLink);
        return liNavItem;
    }
}

