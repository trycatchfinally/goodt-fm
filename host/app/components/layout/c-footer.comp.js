import tmpl from './c-footer.html';

export class CFooterComponent extends HTMLElement {
    constructor() {
        super();
    }

    connectedCallback() {
        this.innerHTML = tmpl;
    }
}
