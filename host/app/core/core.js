import {RouterOutlet}      from '../router/router-outlet';
import {ComponentRegistry} from './component-registry';
import {apps}              from './dispatcher';
import {layoutComponents}  from '../components/layout/index';
import {HomeComponent}     from '../components/home.comp';
import {AboutComponent}    from '../components/about.comp';
import {PostsAppComponent} from '../components/posts-app.comp';
import {PagesAppComponent} from '../components/pages-app.comp';
import {MediaAppComponent} from '../components/media-app.comp';
import {MixedComponent}    from '../components/mixed.comp';

export class Core {
    constructor() {
        if (!Core.inst) {
            Core.inst = this;
        } else {
            throw new Error('use instance');
        }

        ComponentRegistry.register(components);

        return Core.inst;
    }

    static get instance() {
        return Core.inst;
    }
}

Core.inst = null;

const components = [
    ...layoutComponents,
    {
        tagName  : 'router-outlet',
        component: RouterOutlet
    },
    {
        tagName  : 'c-home',
        component: HomeComponent
    },
    {
        tagName  : 'about-app',
        component: AboutComponent
    },
    {
        tagName  : 'mixed-apps',
        component: MixedComponent
    },
    {
        tagName  : apps.PostsApp,
        component: PostsAppComponent
    },
    {
        tagName  : apps.PagesApp,
        component: PagesAppComponent
    },
    {
        tagName  : apps.MediaApp,
        component: MediaAppComponent
    }
];
