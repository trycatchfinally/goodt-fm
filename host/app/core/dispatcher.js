// todo: use enum declaration for TS
export const apps = {
    PostsApp: 'posts-app',
    PagesApp: 'pages-app',
    MediaApp: 'media-app'
};

// todo: extend props, add settings for custom options
const config = {
    [apps.PostsApp]: {
        bundle: '/au-scripts/vendor-bundle.js',
        root  : '[aurelia-app]'
    },
    [apps.PagesApp]: {
        bundle: '/au-scripts/vendor-bundle.js',
        root  : '[aurelia-app]'
    },
    [apps.MediaApp]: {
        bundle: '/vue-scripts/build.js',
        root  : '#vue-media-app'
    }
};

// todo: extend
export class ComponentDispatcher {
    static extract(name) {
        if (!name) return;

        name = name.toLowerCase();
        return config.hasOwnProperty(name) ? config[name] : false;
    }
}
