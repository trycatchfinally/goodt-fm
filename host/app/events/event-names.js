export const e = {
    notification: {
        type: {
            updated: 'component:type:updated'
        },
        outer: {
            event: 'notification:outer:event'
        }
    },
    router      : {
        navigate: {
            trigger: 'router:navigate:trigger'
        },
        extract : {
            root: 'router:extract:root'
        },
        request : {
            root: 'router:request:root'
        }
    }
};
