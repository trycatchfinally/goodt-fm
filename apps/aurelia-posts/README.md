# Aurelia multiple apps showcase
## Main features

* multiple apps
* separated bundles
* isolated styles (via Shadow DOM)
* common event aggregator
* common [event emitter](https://github.com/chrisdavies/eev)

[Demo](https://aurelia-multi-apps.firebaseapp.com/)
