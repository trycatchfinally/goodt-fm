import environment from './../../environment';

export function configure(aurelia) {
  aurelia.use
    .standardConfiguration()
    .feature('resources');

  aurelia.use.developmentLogging(environment.debug ? 'debug' : 'warn');

  if (environment.testing) {
    aurelia.use.plugin('aurelia-testing');
  }

  return aurelia.start().then(() => {
    aurelia.setRoot().then(() => {
      let host = aurelia.host;
      if (Boolean(host.hasAttribute('data-web-component') && host.parentNode)) {
        host.parentNode.setAttribute('initialized', 'true');
      }
    });
  });
}
