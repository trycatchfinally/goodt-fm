import {observable}   from 'aurelia-framework';
import WPAPI          from 'wpapi';
import * as data      from '../../data.json';
import {eventService} from './../../eev-service';

export class App {

  entries;
  @observable outerEventService = null;

  constructor() {
  }

  attached() {
    let wp = new WPAPI({endpoint: data.wp_api_url});

    wp.pages()
      .then(data => {
        this.entries = data;
      })
      .catch(err => {
        console.log('wp pages error', err);
      });

    eventService.on('au.inner.event', () => console.log('event detected: `au.inner.event`'));
  }

  detached() {
  }

  outerEventServiceChanged(newValue, oldValue) {
    if (oldValue === null && this.outerEventService) {
      this.outerEventService.emit('au.outer.event');
    }
  }
}
