import WPAPI          from 'wpapi';
import * as data      from '../../data.json';

export class Authors {

  authors;

  attached() {
    let wp = new WPAPI({endpoint: data.wp_api_url});

    wp.users().perPage(25)
      .then(data => {
        this.authors = data;
      })
      .catch(err => {
        console.log('wp authors error', err);
      });
  }

  detached() {
    this.authors = null;
  }
}
