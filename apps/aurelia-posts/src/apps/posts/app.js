import {
  observable,
  Container,
  Aurelia
}                     from 'aurelia-framework';
import {eventService} from '../../eev-service';

export class App {

  @observable outerEventService = null;

  configureRouter(config, router) {
    let prefix = this.initBaseUrl();

    this.router              = router;
    config.title             = 'Блог';
    config.options.pushState = false;
    config.map([
      {
        route   : [`${prefix}`, `${prefix}/entries`],
        name    : 'entries',
        moduleId: 'apps/posts/entries',
        nav     : true,
        title   : 'Записи'
      },
      {
        route   : `${prefix}/authors`,
        name    : 'authors',
        moduleId: 'apps/posts/authors',
        nav     : true,
        title   : 'Авторы'
      }
    ]);
    config.mapUnknownRoutes('apps/posts/entries');
  }

  attached() {
    eventService.emit('au.inner.event');
  }

  outerEventServiceChanged(newValue, oldValue) {
    if (oldValue === null && this.outerEventService) {
      this.outerEventService.emit('notification:outer:event');
    }
  }

  initBaseUrl = () => {
    let aurelia, host, isWebComponent, prefix = '';

    aurelia = Container.instance.get(Aurelia);
    host           = aurelia.host;
    isWebComponent = Boolean(host.hasAttribute('data-web-component') && host.parentNode);

    if (isWebComponent) {
      host.parentNode.setAttribute('initialized', 'true');
      prefix = host.parentNode.getAttribute('base-url');
    }
    return prefix;
  };
}
