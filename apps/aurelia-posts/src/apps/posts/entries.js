import WPAPI     from 'wpapi';
import * as data from '../../data.json';

export class Entries {

  entries;
  currentEntry;

  attached() {
    let wp = new WPAPI({endpoint: data.wp_api_url});

    wp.posts()
      .then(data => {
        this.entries = data;
      })
      .catch(err => {
        console.log('wp posts error', err);
      });
  }

  detached() {
    this.entries      = null;
    this.currentEntry = null;
  }

  entryDetails = (event, entry) => {
    event.preventDefault();
    this.currentEntry = entry;
  };
}
