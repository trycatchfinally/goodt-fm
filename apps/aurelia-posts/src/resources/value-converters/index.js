export class ObjectKeysValueConverter {
  toView(obj) {
    // Create a temporary array to populate with object keys
    let temp = [];

    // A basic for..in loop to get object properties
    // https://developer.mozilla.org/en/docs/Web/JavaScript/Reference/Statements/for...in
    for (let prop in obj) {
      if (obj.hasOwnProperty(prop) && (typeof obj[prop] === 'string')) {
        temp.push(prop);
      }
    }

    return temp;
  }
}

export class LocaleDateValueConverter {
  toView(value) {
    return new Date(value).toLocaleDateString();
  }
}

export class HtmlToTextValueConverter {
  toView(html) {
    let tmp       = document.createElement('DIV');
    tmp.innerHTML = html;
    return tmp.textContent || tmp.innerText || '';
  }
}
