import axios from 'axios';

export default class DataLoader {

    static init(limit = 25) {
        let url = 'https://blackberries.ru/wp-json/wp/v2/media';

        return new Promise((resolve, reject) => {
            axios.get(url, {params: {per_page: limit}})
                .then(r => resolve(r.data.filter(e => {
                    let details = e['media_details'];
                    return details.width > details.height && (details.width/details.height <= 1.5);
                })))
                .catch(() => reject());
        });
    }
}
