// The following line loads the standalone build of Vue instead of the runtime-only build,
// so you don't have to do: import Vue from 'vue/dist/vue'
// This is done with the browser options. For the config, see package.json
import Vue       from 'vue'
import VueRouter from 'vue-router'
import App       from './App.vue'
import Media     from './components/Media.vue';

Vue.use(VueRouter);

const root = '#vue-media-app';
const router = new VueRouter({
    routes: [
        {path: `/`, name: 'init', component: Media}
    ]
});

new Vue({ // eslint-disable-line no-new
    router,
    data: {
        isWebComponent: document.querySelector(root).hasAttribute('data-web-component') || false,
    },
    el    : root,
    render: (h) => h(App)
});
