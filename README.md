#Фронтенд-микросервисы на основе веб-компонентов, proof of concept

##[Информация о проекте](https://docs.google.com/document/d/1fwxGe85axPMdOwTuGN6BaUg2qd6gExN-dJUqcf0bJkk/edit?usp=sharing) 

Проект содержит поддиректории хост-приложения и приложений на основе Vue и Aurelia. 
Последние могут быть собраны как standalone-приложения, так и компонент-приложения (приложение в обертке веб-компонента):

	./host
	./apps

##Перед началом работы 
Необходимо выполнить установку *aurelia-cli* и, опционально, *vue-cli* версии < 3.0:

	npm install aurelia-cli -g
 	npm install -g vue-cli
 	
Также должен быть установлен *webpack*.

Из текущей директории необходимо выполнить команды, формирующие симлинки:

	ln -s $(pwd)/apps/aurelia-posts/au-scripts/ $(pwd)/host/
	ln -s $(pwd)/apps/vue-media/vue-scripts/ $(pwd)/host/
	
В результате в структуре хост-приложения должны появиться директории, содержащие готовые сборки компонент-приложений.

	./host/au-scripts
	./host/vue-scripts
 
##Сборка
###Компонент-приложение на основе Aurelia
	cd ./apps/aurelia-posts
	npm i
	au build --env prod
	
###Компонент-приложение на основе Vue
	cd ./apps/vue-media
	npm i
	npm run build
	
###Хост-приложение
	cd ./host/
	npm i 
	npm run build
	
##Запуск
В примере для обслуживания финальной сборки задействован pm2:

	cd ./host/dist
	pm2 serve ./ 8080